%include "words.inc"
%include "lib.inc"
extern find_word

section .bss
buffer: resb 256

section .rodata
overflow: db "Overflow. Maximum size is 256", 0
not_found: db "Not found", 0

section .text
global _start

_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	mov rdi, rax
	test rax, rax
	jz .overflow
	mov rsi, next
	call find_word
	test rax, rax
	je .not_found
	mov rdi, rax
	add rdi, 8
	call string_length
	lea rdi, [rdi + rax + 1]
	call print_string
	call print_newline
	mov rax, 0
    jmp exit

.overflow:
	mov	rdi, overflow
	call print_stderr
    call print_newline
    mov rax, 1
    jmp exit
.not_found:
    mov	rdi, not_found
	call print_stderr
    call print_newline
    mov rax, 1
    jmp exit
