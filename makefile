ASM=nasm
ASMFLAGS=-f elf64 -g
LD=ld
RM=rm

program: ../main.o dict.o
	$(LD) -o program $^

main.asm: words.inc dict.asm
	touch $@

dict.asm: lib.inc
	touch $@

words.inc: colon.inc
	touch $@

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

.PHONY: clean

clean:
	$(RM) *.o