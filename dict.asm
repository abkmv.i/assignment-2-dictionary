%include "lib.inc"

section .text

global find_word

find_word:
    xor r8, r8
    .loop:
        push rdi
        push rsi
        add rsi, 8
        call string_equals
        pop rsi
        pop rdi
        cmp rax, 1
        jz .found
        mov r8, [rsi]
        cmp r8, 0
        mov rsi, r8
        jnz .loop
    .not_found:
        mov rax, 0
        ret
    .found:
        mov rax, rsi
        ret